package lt.ito.currencyconverter;

import com.android.volley.RequestQueue;

import java.util.ArrayList;

public class MainActivityPresenter implements DataResponse{
    private DataProvider dataProvider;
    private MainActivityView mainActivityView;

    public MainActivityPresenter (MainActivityView mainActivityView) {
        dataProvider = new DataProvider(this);
        this.mainActivityView = mainActivityView;
    }

    public void getDataFromApi (RequestQueue requestQueue) {
        dataProvider.getDataFromApi(requestQueue);
    }

    @Override
    public void onDataReceived(ArrayList<DataModel> data) {
        mainActivityView.onDataSuccess(data);
    }

    @Override
    public void onErrorReceived(String message) {
        mainActivityView.onDataFailed(message);
    }
}
