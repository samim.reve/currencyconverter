package lt.ito.currencyconverter;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DataModel {

    String countryName;
    ArrayList<HashMap<String, Object>> taxModule;

    public String getCountryName() {
        return countryName;
    }

    public ArrayList<HashMap<String, Object>> getTaxModule() {
        return taxModule;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setTaxModule(ArrayList<HashMap<String, Object>> taxModule) {
        this.taxModule = taxModule;
    }
}
