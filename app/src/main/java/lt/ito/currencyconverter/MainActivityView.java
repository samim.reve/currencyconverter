package lt.ito.currencyconverter;

import java.util.ArrayList;

public interface MainActivityView {

    void onDataSuccess(ArrayList<DataModel> data);
    void onDataFailed(String message);
}
