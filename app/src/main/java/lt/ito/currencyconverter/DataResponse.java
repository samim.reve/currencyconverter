package lt.ito.currencyconverter;

import java.util.ArrayList;

public interface DataResponse {
    void onDataReceived(ArrayList<DataModel> data);
    void onErrorReceived(String message);
}
