package lt.ito.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private RequestQueue dataRequestQueue;
    private MainActivityPresenter mainActivityPresenter;
    private EditText amountEt;
    private Spinner countryListSpinner, effectiveDateSpinner, taxTypesSpinner;
    private TextView apiStatus;
    private ProgressBar progressBar;
    private ArrayAdapter<String> countryListAdapter, effectiveDateAdapter, taxTypesAdapter;
    private ArrayList<DataModel> apiData;
    private LinearLayout taxTypesCont;
    private TextView calculatedTax;
    private double taxRates = 0;
    private LinearLayout countryListCont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initComponents();
        mainActivityPresenter.getDataFromApi(dataRequestQueue);
    }

    private void initComponents() {
        dataRequestQueue = Volley.newRequestQueue(this);
        mainActivityPresenter = new MainActivityPresenter(this);
        apiData = new ArrayList<>();
    }

    private void initViews() {
        amountEt = findViewById(R.id.amountET);
        countryListSpinner = findViewById(R.id.countryList);
        apiStatus = findViewById(R.id.apiStatus);
        progressBar = findViewById(R.id.dataProgress);
        taxTypesCont = findViewById(R.id.taxTypesCont);
        effectiveDateSpinner = findViewById(R.id.effectivePeriods);
        taxTypesSpinner = findViewById(R.id.taxTypes);
        calculatedTax = findViewById(R.id.calculatedTax);
        countryListCont = findViewById(R.id.countryListCont);

        amountEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (amountEt.getText().toString().trim().length() == 0) {
                    calculatedTax.setText("0.00");
                } else {
                    showCalculatedTax();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    protected void onDestroy() {
        super.onDestroy();
        dataRequestQueue.cancelAll("DATA");
    }

    @Override
    public void onDataSuccess(ArrayList<DataModel> data) {
        apiData = data;
        loadCountryNames();
        progressBar.setVisibility(View.GONE);
        apiStatus.setVisibility(View.GONE);
        countryListCont.setVisibility(View.VISIBLE);
        taxTypesCont.setVisibility(View.VISIBLE);


    }

    private void loadCountryNames () {
        ArrayList<String> countryName = new ArrayList<>();
        for (int i=0; i<apiData.size();i++) {
            countryName.add(apiData.get(i).getCountryName());
        }
        countryListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, countryName);
        countryListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryListSpinner.setAdapter(countryListAdapter);
        if (countryName.contains("Lithuania")) {
            countryListSpinner.setSelection(countryName.indexOf("Lithuania"));
        }

        countryListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadEffectiveDates(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void loadEffectiveDates(final int arrayIndex) {
        ArrayList<String> effectiveDates = new ArrayList<>();
        DataModel dataModel;
        dataModel = apiData.get(arrayIndex);
        for (int i=0; i<dataModel.getTaxModule().size(); i++) {
            effectiveDates.add((String) dataModel.getTaxModule().get(i).get("effective_from"));
        }
        effectiveDateAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, effectiveDates);
        effectiveDateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        effectiveDateSpinner.setAdapter(effectiveDateAdapter);

        effectiveDateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadTaxTypes(arrayIndex, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadTaxTypes (int arrayIndex, int position) {
        try {
            ArrayList<String> taxTypes = new ArrayList<>();
            final ArrayList<Double> taxTypesValues = new ArrayList<>();
            DataModel dataModel;
            dataModel = apiData.get(arrayIndex);
            JSONObject taxObject = (JSONObject)dataModel.getTaxModule().get(position).get("rates");
            Iterator<String> keys = taxObject.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                double value = taxObject.getDouble(key);
                taxTypes.add(key.toUpperCase() + " [" + value + "%]");
                taxTypesValues.add(value);
            }

            taxTypesAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_item, taxTypes);
            taxTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            taxTypesSpinner.setAdapter(taxTypesAdapter);

            taxTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    taxRates = taxTypesValues.get(position);
                    showCalculatedTax();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDataFailed(String message) {
        countryListCont.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        apiStatus.setVisibility(View.VISIBLE);
        apiStatus.setText(message);
        taxTypesCont.setVisibility(View.GONE);
    }

    private void showCalculatedTax () {
        try {
            Double amount = Double.valueOf(amountEt.getText().toString().trim());
            String result = "" + (amount - (amount*(taxRates/100)));
            calculatedTax.setText(result);
        } catch (Exception e) {
            e.printStackTrace();
            calculatedTax.setText("0.00");
        }

    }
}
