package lt.ito.currencyconverter;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class DataProvider {
    private DataResponse dataResponse;
    private ArrayList<DataModel> data;

    public DataProvider(DataResponse dataResponse) {
        this.dataResponse = dataResponse;
    }

    public void getDataFromApi(RequestQueue requestQueue) {
        String api = "https://jsonvat.com/";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(api, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.toString().length() > 0) {
                    try {
                        loadDataToArray(response.getJSONArray("rates"));
                        dataResponse.onDataReceived(data);
                    } catch (Exception e) {
                        e.printStackTrace();
                        dataResponse.onErrorReceived(e.getMessage());
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dataResponse.onErrorReceived(error.getMessage());
            }
        });

        jsonObjectRequest.addMarker("DATA");
        requestQueue.add(jsonObjectRequest);
    }

    private void loadDataToArray(JSONArray jsonArray) {
        data = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                DataModel dataModel = new DataModel();
                dataModel.setCountryName(jsonArray.getJSONObject(i).getString("name"));
                ArrayList<HashMap<String, Object>> taxModule = new ArrayList<>();
                JSONArray periods = jsonArray.getJSONObject(i).getJSONArray("periods");
                for (int j=0; j<periods.length(); j++) {
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("effective_from", (String)periods.getJSONObject(j).getString("effective_from"));
                    hashMap.put("rates", (JSONObject) periods.getJSONObject(j).getJSONObject("rates"));
                    taxModule.add(hashMap);
                }
                dataModel.setTaxModule(taxModule);
                data.add(dataModel);

                Collections.sort(data, new Comparator<DataModel>() {
                    @Override
                    public int compare(DataModel o1, DataModel o2) {
                        return o1.getCountryName().compareTo(o2.getCountryName());
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
